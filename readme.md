# T4 Versioing #
A Sample project using T4 versioing to implement a version based on a julian
year and UTC timestamp for Build Number and revision.  Any C# code could be used.

## Motivation ##
To provide a build numbering system that is pretty much build into Visual Studio.

## Caveats ##
- Modifying the assembly info via solution properties will modify the generated
AssemblyInfo.cs. Instead of the template.  You can copy and paste from the .cs 
file to the .tt file pretty easilly however as the .cs is not generated until 
the template is saved or a build is run.
- AssemblyInfo.cs will be regenerated during debug or release builds.  
TextTransform.exe generally completes pretty quickly however.

## Setup ##
- C#  
 - Rename AssemblyInfo.cs to AssemblyInfo.tt
 - Add a Pre-build command of  
    "C:\Program Files (x86)\Common Files\Microsoft Shared\TextTemplating\14.0\TextTransform.exe" ^
    $(ProjectDir)Properties\AssemblyInfo.tt
- C++  
    TBD